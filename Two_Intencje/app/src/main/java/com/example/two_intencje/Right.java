package com.example.two_intencje;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Right extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right);
        Intent intent = getIntent();
        String messageText = intent.getStringExtra("message");
        TextView textView = findViewById(R.id.textxview_display);
        textView.setText(messageText);
    }

    public void onClickMiddle(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onClickLeft(View view){
        Intent intent = new Intent(this, Left.class);
        startActivity(intent);
    }
}
