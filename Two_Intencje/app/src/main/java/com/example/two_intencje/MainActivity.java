package com.example.two_intencje;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editText);
    }

    public void onClickLeft(View view){
        Intent intent = new Intent(this, Left.class);
        startActivity(intent);
    }

    public void onClickRight(View view){
        String message = editText.getText().toString();
        Intent intent = new Intent(this, Right.class);
        intent.putExtra("message", message);
        startActivity(intent);
    }

    public void onClickMiddle(View view){
        String message = editText.getText().toString();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(intent);
    }
}
