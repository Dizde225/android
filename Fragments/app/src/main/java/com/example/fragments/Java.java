package com.example.fragments;

public class Java {
    private String tittle;
    private int quantity;

    public final static Java[] javas = {
            new Java("first", 12),
            new Java("second", 12),
            new Java("third", 23)
    };

    public Java(String tittle, int quantity) {
        this.tittle = tittle;
        this.quantity = quantity;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
