package com.example.four_multi_level;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ProgrammingDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "languages";
    private static final int DB_VERSION = 1;

    public ProgrammingDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE LANGUAGES (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "CATEGORY TEXT, " +
                "NAME TEXT, " +
                "DESCRIPTION TEXT)");
        insertLanguage(db, "Java", "TEST","List of elements");
        insertLanguage(db, "Java", "Basic types","integers, floating-point numbers, booleans");
        insertLanguage(db, "Java", "Strings","Sequence of charachters");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static void insertLanguage(SQLiteDatabase db, String cat, String name, String description){
        ContentValues programValue = new ContentValues();
        programValue.put("CATEGORY", cat);
        programValue.put("NAME", name);
        programValue.put("DESCRIPTION", description);
        db.insert("LANGUAGES", null, programValue);
    }
}
