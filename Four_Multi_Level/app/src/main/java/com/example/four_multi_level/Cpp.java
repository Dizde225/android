package com.example.four_multi_level;

public class Cpp {
    private String name;
    private String description;

    public static final Cpp[] functions = {
            new Cpp("Arrays", "List of elements"),
            new Cpp("Basic types", "integers, floating-point numbers, booleans"),
            new Cpp("Strings", "Sequence of charachters")
    };

    public Cpp(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }
}
