package com.example.four_multi_level;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class JavaCategoryActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<Java> listAdapter = new ArrayAdapter<Java>(
                this,
                android.R.layout.simple_list_item_1,
                Java.functions
        );

        ListView listView = getListView();
        listView.setAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(JavaCategoryActivity.this, JavaActivity.class);
        intent.putExtra(JavaActivity.EXTRA_JAVA, (int) id);
        startActivity(intent);
    }
}
