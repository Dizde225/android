package com.example.four_multi_level;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CppActivity extends Activity {

    public static final String EXTRA_CPP = "cppNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_python_details);

        int cppNo = (int) getIntent().getExtras().get(EXTRA_CPP);
        Cpp cpp = Cpp.functions[cppNo];

        TextView tittle = findViewById(R.id.tittle);
        tittle.setText(cpp.getName());

        TextView description = findViewById(R.id.description);
        description.setText(cpp.getDescription());

        ImageView imageView = findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.cpp);
    }
}
