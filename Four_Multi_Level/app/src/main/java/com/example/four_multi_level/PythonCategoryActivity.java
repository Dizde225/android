package com.example.four_multi_level;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PythonCategoryActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<Python> listAdapter = new ArrayAdapter<Python>(
                this,
                android.R.layout.simple_list_item_1,
                Python.functions
        );

        ListView listView = getListView();
        listView.setAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(PythonCategoryActivity.this, PythonActivity.class);
        intent.putExtra(PythonActivity.EXTRA_PYTHON0, (int) id);
        startActivity(intent);
    }
}
