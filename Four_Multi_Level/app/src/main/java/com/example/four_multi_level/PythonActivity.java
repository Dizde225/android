package com.example.four_multi_level;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PythonActivity extends Activity {

    public static final String EXTRA_PYTHON0 = "pythonNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_python_details);
        int pythonNo = (int) getIntent().getExtras().get(EXTRA_PYTHON0);

        try{
            SQLiteOpenHelper sqLiteOpenHelper = new ProgrammingDatabaseHelper(this);
            SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
            Cursor cursor = db.query("LANGUAGES",new String[] {"CATEGORY", "NAME", "DESCRIPTION"},
                    "_id=?",
                    new String[] {Integer.toString(pythonNo+1)},
                    null,null,null);


            if (cursor.moveToFirst()){
                String categoryText = cursor.getString(0);
                String nameText = cursor.getString(1);
                String descText = cursor.getString(2);

                TextView tittle = findViewById(R.id.tittle);
                tittle.setText(nameText);

                TextView description = findViewById(R.id.description);
                description.setText(descText);
            }
            cursor.close();
            db.close();
        } catch (SQLException e){
            Toast tost = Toast.makeText(this, "Baza danych jest niedostępna", Toast.LENGTH_SHORT);
            tost.show();
        }

        ImageView imageView = findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.python);
    }
}
