package com.example.four_multi_level;

public class Python {
    private String name;
    private String description;

    public static final Python[] functions = {
            new Python("Arrays", "List of elements"),
            new Python("Basic types", "integers, floating-point numbers, booleans"),
            new Python("Strings", "Sequence of charachters")
    };

    public Python(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }
}
