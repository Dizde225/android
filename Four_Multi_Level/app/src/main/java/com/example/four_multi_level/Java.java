package com.example.four_multi_level;

public class Java {
    private String name;
    private String description;

    public static final Java[] functions = {
            new Java("Arrays", "List of elements"),
            new Java("Basic types", "integers, floating-point numbers, booleans"),
            new Java("Strings", "Sequence of charachters")
    };

    public Java(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }
}
