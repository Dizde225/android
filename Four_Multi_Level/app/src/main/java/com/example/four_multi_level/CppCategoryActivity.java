package com.example.four_multi_level;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class CppCategoryActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayAdapter<Cpp> listAdapter = new ArrayAdapter<Cpp>(
                this,
                android.R.layout.simple_list_item_1,
                Cpp.functions
        );

        ListView listView = getListView();
        listView.setAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(CppCategoryActivity.this, CppActivity.class);
        intent.putExtra(CppActivity.EXTRA_CPP, (int) id);
        startActivity(intent);
    }
}
