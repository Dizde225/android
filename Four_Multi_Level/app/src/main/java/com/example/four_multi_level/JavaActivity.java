package com.example.four_multi_level;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class JavaActivity extends Activity {

    public static final String EXTRA_JAVA = "javaNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_python_details);

        int javaNo = (int) getIntent().getExtras().get(EXTRA_JAVA);
        Java java = Java.functions[javaNo];

        TextView tittle = findViewById(R.id.tittle);
        tittle.setText(java.getName());

        TextView description = findViewById(R.id.description);
        description.setText(java.getDescription());

        ImageView imageView = findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.java);
    }
}
