package com.example.fragment_test;

public class Workout {
    private String name;
    private String description;

    public static final Workout[] workouts = {
            new Workout("pompki","na rekach"),
            new Workout("test1","testujemy sobie"),
            new Workout("Przysiady", "na nogach")
    };

    public Workout(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
