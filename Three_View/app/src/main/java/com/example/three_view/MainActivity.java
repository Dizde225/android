package com.example.three_view;

import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    Button goToFrameButton;
    Switch wifiSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        goToFrameButton = findViewById(R.id.go_to_frame_button);
        wifiSwitch = findViewById(R.id.switch1);
    }

    public void onClickFrame(View view) {
        Intent intent = new Intent(this, frame_activity.class);
        startActivity(intent);
    }

    public void onClickRadio(View view){
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        int duration = Toast.LENGTH_SHORT;
        int id = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(id);
        Toast toast = Toast.makeText(this, radioButton.getText(),duration);
        toast.show();
    }

    public void onSwitchClick(View view) {
        boolean on = wifiSwitch.isChecked();
        String text = "";
        int duration = Toast.LENGTH_SHORT;
        if (on) {
            text = getResources().getString(R.string.wifi_on);
        } else {
            text = getResources().getString(R.string.wifi_off);
        }
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    public void onButtonCheckbox(View view){
        String text = "";
        CheckBox checkBox1 = findViewById(R.id.checkBox);
        CheckBox checkBox2 = findViewById(R.id.checkBox2);
        CheckBox checkBox3 = findViewById(R.id.checkBox3);
        int duration = Toast.LENGTH_SHORT;

        if(checkBox1.isChecked()){
            text += checkBox1.getText() + " ";
        }
        if(checkBox2.isChecked()){
            text += checkBox2.getText() + " ";
        }
        if(checkBox3.isChecked()){
            text += checkBox3.getText() + " ";
        }
        if (text.equals("")){
            text = "Check Something!";
        }
        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }
}

