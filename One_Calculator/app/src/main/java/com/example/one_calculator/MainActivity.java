package com.example.one_calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.security.Principal;

public class MainActivity extends AppCompatActivity {

    private Spinner numbersOne;
    private Spinner numbersTwo;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numbersOne = findViewById(R.id.numbers_one);
        numbersTwo = findViewById(R.id.numbers_two);
        result = findViewById(R.id.result_textview);
    }

    public void onClickPlus(View view){
        float [] num = getNumbers();
        result.setText("Result:\t" + (num[0] + num[1]));
    }

    public void onClickMinus(View view){
        float [] num = getNumbers();
        result.setText("Result:\t" + (num[0] - num[1]));
    }

    public void onClickMultiply(View view){
        float [] num = getNumbers();
        result.setText("Result:\t" + (num[0] * num[1]));
    }

    public void onClickDivision(View view){
        float [] num = getNumbers();
        if (num[1] == 0){
            result.setText("You can't divide by 0");
        } else {
            result.setText("Result:\t" + (num[0] / num[1]));
        }
    }

    public void onClickClear(View view){
        result.setText(getString(R.string.result_text));
    }

    private float[] getNumbers(){
        float numberOne = Float.parseFloat(numbersOne.getSelectedItem().toString());
        float numberTwo = Float.parseFloat(numbersTwo.getSelectedItem().toString());
        float [] numbers = {numberOne, numberTwo};
        return numbers;
    }
}
